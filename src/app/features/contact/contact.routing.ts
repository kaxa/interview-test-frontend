import {Routes, RouterModule} from '@angular/router';
import {ContactComponent} from "./contact.component";


export const contactRoutes: Routes = [
  {path: '', component: ContactComponent},
];

export const ContactRouting = RouterModule.forChild(contactRoutes);
