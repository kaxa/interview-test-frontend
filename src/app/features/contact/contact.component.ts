import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ContactService} from "../../core/services/contact/contact.service";
import {SubmitContactFormModel} from "../../core/shared.models";
import {Router} from "@angular/router";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  emailRegx = /^(([^<>+()\[\]\\.,;:\s@"-#$%&=]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
  contactForm: FormGroup = this.formBuilder.group({
    name: [null, [Validators.required]],
    surName: [null, [Validators.required]],
    email: [null, [Validators.required, Validators.pattern(this.emailRegx)]],
    position: [null, [Validators.required]],
    birthDate: [null, [Validators.required]],
    comment: [null, [Validators.required]],
  });

  formSubmitError: string | undefined;

  constructor(private formBuilder: FormBuilder, private contactService: ContactService, private router: Router) {
  }

  ngOnInit(): void {
  }

  submit() {

    if (!this.contactForm.valid) {
      return;
    }

    this.formSubmitError = undefined;
    const submitContactFormModel: SubmitContactFormModel = {
      name: this.contactForm.value.name,
      comment: this.contactForm.value.comment,
      surName: this.contactForm.value.surName,
      position: this.contactForm.value.position,
      email: this.contactForm.value.email,
      birthDate: new Date(this.contactForm.value.birthDate).toDateString()
    }

    this.contactService.submitContact(submitContactFormModel).subscribe(data => {
      this.router.navigate(['/home']).then();
    }, error => {
      console.log(error);
      this.formSubmitError = error.error.message;
    });

  }

}
