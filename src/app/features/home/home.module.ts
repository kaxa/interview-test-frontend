import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {HomeRouting} from "./home.routing";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRouting,
    MatIconModule,
    MatButtonModule
  ]
})
export class HomeModule {
}
