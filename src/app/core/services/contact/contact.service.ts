import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SubmitContactFormModel, SubmitContactResponse} from "../../shared.models";
import {environment} from "../../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private http: HttpClient) {
  }

  submitContact(contact: SubmitContactFormModel) {
    return this.http.post<SubmitContactResponse>(environment.serverUrl + '/contact', contact, {
      headers: {'Access-Control-Allow-Origin': '*'}
    });
  }

}
