export interface SubmitContactFormModel {
  name: string,
  surName: string,
  email: string,
  position: string,
  birthDate: string,
  comment: string
}

export interface SubmitContactResponse {
  id: number;
  name: string,
  surName: string,
  email: string,
  position: string,
  birthDate: string,
  comment: string
}
