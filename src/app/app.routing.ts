import {Routes, RouterModule} from '@angular/router';
import { NgModule } from '@angular/core';

export const appRoutes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: 'home', loadChildren: () => import('../app/features/home/home.module').then(m => m.HomeModule)},
  {path: 'contact', loadChildren: () => import('../app/features/contact/contact.module').then(m => m.ContactModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
